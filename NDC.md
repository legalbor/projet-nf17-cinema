## Liste des classes:
    NB:On ne tient pas compte ici des clés étrangères qui seront des attribus en plus (voir MLD)

* Film contient titre, date de sortie,age minimum,duree
    
    Durée est indiqué en secondes

* Realisateur contient nom, prenom, age
* Producteur contient nom, prenom, age
* genre contient un nom


* Seance contient le nombre de place vendues, le jour, l'heure et le doublage.

    N'ayant pas plus de précisions sur le doublage dans le cahier des charges, on supposera que c'est simpklement une chaine de caractère quelquonque comme VO ou VOST,
    mais il n'y aura pas de contraintes suplémentaires si on veut projeter un film en italien non soutitré par exemple.

* Salle contient un numero et un nombre de place total

* Distributeur contient un nom


* Entree contient un identifiant unique

    Entree peut etre de deux type,titre unitaire et carte d'abonnement qui sont deux classes filles de entrée, et ne peut etre instancié, c'est donc une classe abstact

* Titre_unitaire contient le tarif, qui est une chaine de charactère au choix du cinéma, il n'y a donc pas de contrainte dessus. 
 
        /!\ Modification, avec la venue du JSON, on ajoutera une valeur en euro au tarif.
    
* Carte d'abonnement contient le nombre de place restantes sur la carte, ainsi qu'une méthode permettant d'incrémenter le nb de place restante, et de les décrémenter lors de l'cahat d'une entree spécifique (voir remarques).Il peut y avoir jusqu'a 20 places restantes(rechargement max).
    
* Note contient un chiffre entier entre 0 et 5

* Vendeur contient un nom, prenom et un age.

* Produit contient prix et nom 
    
    Produit est la classe mère de boisson et alimentaire, c'est une classe abstraite

* Boisson herite de Produit

* Alimentaire herite de produit
    
## Associations entres les classes:

1 Séance à Lieu dans 1 salle.

1 Film est Diffusé dans N Séances, mais une séance n'existe pas sans film, on a donc une composition.

\* Film est réalisé par 1...Réalisteur

\* Film est produit par 1...N producteurs


\* Film est categorisé par 1...N genres(action, comedie, drame etc), car un film peut avoir pluesieurs genres différents, ex : action-comédie/horreur-drame etc

1 Distributeur gere N Films


1 Film possede N Notes, mais une note n'existe pas sans film, on a donc une composition.

N titres unitaires permettent l'acces à 1 Séance chacun,  mais un titre unitaire n'existe pas sans séance, on a donc une composition.

N cartes d'abonnement permettent l'acces à N Séances. (Voir remarque pour plus de details)

1 titre unitaire est vendue par 1 Vendeur

N carte d'abonnement peuvent etre débitée par M vendeurs

N Vendeurs vendent N produits(Boisson ou Alimentaire)

Pour toutes les associations de type N..M, je vous invite a aller voir la MLD pour plus de détails.

## Heritage et implémentation

Pour les classes Entree, Carte\_abonement et Tarif\_unitaire, on choisira un héritage par classe fille, car Entree est une classe abstraite qui ne sera jamais instanciée,
et les classes fille n'absorberont qu'un seul argument qui sera leur clé primaire (voir si dessous).


Pour les classes Produit, Boisson et Alimentaire, on choisira un héritage par classe mère, car les classes filles ne contienent aucun argument. 
On implémentera donc un argument qui définira le statut du produit.

__Voir Vues pour plus de détails sur la restitution.__

## DF et clés
        NB : Valide avant JSON, quelques modification mineures ont lieu après, voir MLD pour plus de détails.
### DF
Le titre d'un film et sa date de sortie seront supposés uniques. L'hypothèse de deux films ayant le même nom et la même date de sortie est écartée.

On a donc la dépendance fonctionnelle de Titre+film -> Age limite, duréé,notes,réalistateurs, producteurs et distributeur.

Pour réalisateur, et producteur, on supposera que les homonymes avec meme nom meme prenom peuvent exister pour éviter tout probleme. Il faudra donc des identifiants pour les différencier.


De la même manière, on supposera que deux distributeurs ne peuvent pas avoir le même nom, ca serait mauvais pour les affaires...

Le nom d'un genre est unique.

Une séance peut être identifiée par le jour l'heure et la salle, car on ne peut pas avoir deux séance en même temps au même endroit.

On a donc la DF  jour, heure,salle->film, doublage, et le nombre de places vendues+les titres unitaires pour ce film plus les cartes d'abonnements ayant permis l'acces à cette séance.

Pour la salle,on a grâce à l'unicité du numéro(il n'y a pas 2 salles 3 dans le cinéma):

numero->nb places dispos

Pour vendeur, on supposera qu'on peut avoir des homonymes, meme si on travaille dans une petite ville... On utilisera donc des  Ids.


Pour les produit, on supposera qu'on ne peut pas avoir deux produit ayant le même nom. On aura par exemple Pop COrn moyen, Pop corn grand etc pour différencier les produits.
Nom->tarif


### Cle primaire

Suite à l'analyse des Dfs, on peut en déduire la clé primaire pour les classes suivantes:

* Film: #nom,#duree
* Realisateur: #idR
* Producteur: #idP
* Distributeur: #nom
* Note: #id_Note
* Genre: #nom
* Salle: #numero
* Titre_unitaire: #id
* Carte_Abonnement : #id
* Seance: #Salle,#jour,#heure
* Vendeur: #idV
* Produit: #Nom

Les clés primaires intervenant dans la MLD pour les relations N..M ne sont pas spécifiées ici, car elles ne necessitent pas d'explications.

### Normalisation
Dans toutes les classes:

(Avant JSON)
* Nous n'avons que des attributs atomiques(1 NF)
* Tout attribut n'appartenant à aucune clé candidate ne dépend pas d'une partie seulement d'une clé candidate (2NF).
* Tout attribut n'appartenant à aucune clé candidate ne dépend directement que de clés candidates (3NF).

          Nous somme donc en 3NF 
          
__/!\Apres internvention du JSON, certains attributs de sont plus atomiques, nous ne somme donc plus en 1 NF, donc "0 NF".__

## Vues et select
* Liste de toutes les boissons par prix croissant
* Lsite de tous les produits alimentaires par prix croissant
* Liste de tous les produit avec nom plus prix, triés par prix (on suppose par exemple que le client sait si Fanta est une boisson ou nom)
* Pourcentage occupation pour chaque seance
* Note moyenne pour chaque film
* Part des abonnés et des tarifs unitaires sur l'ensemble des entrées vendues
* Vue qui liste tous les tiquets ayant pour acces X films

*Avec JSON, nous avons 4 nouvelles vues car les tiquets unitaires contiennet maintenant un prix.*

* Revenu total, qui additionne tous les prix des tiquets vendus plus multiplie le nombre d'entree grace à une carte par le prix d'une entree par carte (cad 6€) et additione les deux.
* Revenu par film pour les titres unitaires, qui somme les prix des différents films selon les prix (on fait un inner join sur la clé étrangere de tiquet pour relier les tiquets avec les séance et donc le titre et la date de sortie, et group by pour grouper par seance)
* Revenu par film pour les cartes d'abonnement, qui nous donne le nombre d'entree par carte d'abonnement pou un film en particulier. La même technique que la vue précédente est utilisée, on cghange juste la methode sum() par la methode count()
* Revenu total par film qui comme son nom l'indique donne le revenu total classé par film. On s'appuie pour cela sur les deux vues précédentes.


Etant donné que les prix des entrées n'ont pas été défini, ni le montant des tarifs on ne calculera pas les revenus. (Après l'avis d u responsable de TD de 
garder "un modele clair et non ambigüe. Essayer de vous tenir aux consignes le plus possible", je n'ai pas ajouté de classes supplémentaires pour complexifier le sujet donné)

## Gestion des droits/acces

On part sur une structure hiérarchique simple qui pourrait être changée en fonction des gérants et de l'oganisation hiérarchique du cinema en lui même. 
Nous crérons des groupes en SQL pour pouvoir assigner à plusieurs utilisateurs du meme types les mêmes droits (ex :tous les vendeurs pourront faire la même chose).

-Le premier type d'utilisateur est le responsable informatique qui a tous les droits possibles afin de gérer tout incident/modification.

-Le second est gérant du ciméma.
Cet utilisateur à un droit de lecture et d'écriture sur toute les tables, pour mettre à jour tous les catalogues et éventuellement résoudre des problème simple de données (duplication etc).
En tant que gérant, il va peut être être amené à effectuer des modifications. Il est donc autorisé à faire des Updates, delete(avec des where). 
Il n'est cependant pas authorisé à alterer les tables (ALTER responsabilité du gérant info). Il peut lui aussi vendre des produits de la même facon que les vendeurs.


-Le troisieme utilisateur, le vendeur. Il peut consulter les différents catalogues de film/séance/produit pour renseigner les clients. Il ne peut rien modifier de ces catalogues mais on doit cependant lui donner
l'acces à des updates pour modifier les cartes abonnements et le nombre de personnes dans une séance. On doit aussi lui permettre de créer des tiquets unitaires, des nvelles cartes et Rel carte pour lier une carte d'abonnement à une séance.

-Enfin on peut imaginer un quatrièeme utilisateur, le client, qui pourrait vouloir consulter la liste des séances, ou la liste des produits pour faire des réservations. De telles consultations 
pourraient se faire sur un hypothétique site internet ou sur des bornes dans le cinema. Ces clients devraient alors avoir acces à des SELECT pour avoir accès à des données spécifiques. Il peuvent également noter des films 
à travers les bornes prévues à cet effet (ou le site internet si ily en a un).

    NB: La couche applicative se chargera de limiter les accès de chacun. Par exemple les vendeurs ne pourront qu'insert dans la table tiquet et Relcarte,
    update des cartes et le champs nombre personne dans un séance sera modifié automatiquement à l'ajout d'un Rel carte ou d'un tiquet. La personne en
    elle même ne fera pas de modification, mais des modifications à son nom seront faites automatiquement par la couche applicative. Il en est de même 
    pour les gérants du cinema.




## Remarques particulières:

* Le cas de la carte d'abonnement est un peu spécial. Un vendeur peut permetre l'acces à une nouvelle séance, et en retirant un point. 
Son nom est alors sauvegardé sur la carte lui aussi. Ainsi, la carte permet l'acces à une nouvelle séance, et on garde la trace du vendeur ayant vendu la place pour cette séance.
Toutes les séances rendues accessibles seront conservées, ainsi que les vendeurs associés.
L'association avec séance n'est donc pas une composition car la carte ne disparait pas si la séance associé disparait, et la carte peut ne contenir aucune séance au moment de l'achat.
On ne peut prendre qu'une entree par carte par seance, impossible de debiter 5 places pour une seance sur une seule carte (voir MLD pour precision). 

* Le vendeur peut aussi recharger la carte. Le retrait et l'ajout de place se fait par une couche externe à la base de donéée(par exemple fonction python dans notre cas).

* Le nombre d'entrees vendues pour une seance doit etre inferieur au nombre maximal de personnes dans la salle, mais cela est aussi géré en externe,
aucune entrée ne sera ajoutée si on atteint nb place restantes==nb places vendues. Le nombre de personne par séance est aussi géré sur la couche externe, les exemples d'insert ne sont donc pas corrects à ce niveau. Dans les inserts on a plus de personne dans une séance que de tiquet/carte d'abonnement reliés à la seance. On pourrait décider de changer cela en implementant une vue qui compte le nombre d'entrées, mais ce n'est pas le choix qui a été retenu.



